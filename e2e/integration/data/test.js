// baidu.js

describe('第一个测试脚本', function() {
    it("百度输入框功能", function() {
        cy.visit('https://www.baidu.com')     // 打开百度网站
        cy.get('#kw').type(Cypress.env('username'))         // 获取搜索框并输入Cypress
            .should('have.value', Cypress.env('username'))  // 断言文本成功输入
            .clear()                          // 清空输入框
            .should('have.value', '')         // 断言文本已成功清除
    })
})

// describe    声明一个测试用例集
// beforeEach  测试用例前置操作，相当于setup
// it          声明一个测试用例
// cy.get      定位元素，用css selector定位选择器
// type        输入文本
// should      断言，have.value是元素的value属性值
// clear       清空文本 