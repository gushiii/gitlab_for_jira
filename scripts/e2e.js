//引入本地模块
const cypress = require('cypress');
const fse = require('fs-extra');
const { merge } = require('mochawesome-merge');
const generator = require('mochawesome-report-generator');

async function runTests() {
     await fse.remove("e2e/results");
     //移除之前生成的报告目录
    await fse.remove('mochawesome-report');
    const { totalFailed } = await cypress.run({
        record: true,
        key: "910296ac-c196-461f-84a4-a7eacdaf703c",
        env: {
            "username": "admin",
            "password": "admin"
        }
    });
    const jsonReport = await merge({
        files:['./e2e/results/*.json']
    });
    await generator.create(jsonReport);
    process.exit(totalFailed)
}
runTests();